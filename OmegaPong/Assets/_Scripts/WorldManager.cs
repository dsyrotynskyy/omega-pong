﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace OmegaPong
{
    public class WorldManager : UnitySingleton<WorldManager>
    {
        [SerializeField]
        private Transform ballSpawnPosition;
        [SerializeField]
        private Ball ballPrefab;
        [SerializeField]
        private PlayerRacket[] allPlayers;

        private PlayerRacket _starterPlayer;
        private Ball _ballInstance;

        private void Awake()
        {
            if (PhotonNetwork.room == null) // check if we connected, otherwise let's start offline mode 
            {
                PhotonNetwork.offlineMode = true;
            }
            if (PhotonNetwork.offlineMode)
            {
                PhotonNetwork.CreateRoom("Offline room");
            }
        }

        void Start()
        {
            if (PhotonNetwork.isMasterClient)
            {
                StartGameRound(allPlayers[0]);
            }
        }

        void StartGameRound (PlayerRacket playerStarter)
        {
            _starterPlayer = playerStarter;
            _starterPlayer.AddOnHasMovedListener(OnStarterPlayerHasMoved);
            foreach (var player in allPlayers)
            {
                player.Master_OnRoundStart();
            }
        }

        internal PlayerRacket GetPlayer(PlayerId playerId)
        {
            for (int i = 0; i < allPlayers.Length; i++) // not oMEGA fast implementation yeah...
            {
                if (playerId == allPlayers[i].Id)
                {
                    return allPlayers[i];
                }
            }
            Debug.LogError("Missing player with id = " + playerId);
            return null;
        }

        private void OnStarterPlayerHasMoved()
        {
            Vector2 dir;
            if (_starterPlayer.Id == PlayerId.Player1)
            {
                dir = Vector2.right;
            }
            else
            {
                dir = Vector2.left;
            }

            SpawnBall(dir, ballSpawnPosition.position);
        }

        void SpawnBall (Vector2 direction, Vector2 spawnPosition)
        {
            if (PhotonNetwork.offlineMode)
            {
                PhotonNetwork.InstantiateInRoomOnly = false;
            }

            _ballInstance = PhotonNetwork.Instantiate(ballPrefab.name, spawnPosition, new Quaternion (), 0).GetComponent<Ball> ();

            _ballInstance.Master_SetDirection(direction);
            _ballInstance.onTouchedPlayerWall += OnTouchedPlayerWall;
        }

        private void OnTouchedPlayerWall(PlayerId wallId)
        {
            PlayerId winnerPlayerId;
            if (wallId == PlayerId.Player1)
            {
                winnerPlayerId = PlayerId.Player2;
            } else
            {
                winnerPlayerId = PlayerId.Player1;
            }

            PlayerRacket playerWinner = GetPlayer(winnerPlayerId);
            playerWinner.Master_OnWonRound ();

            StartCoroutine(RespawnBall (playerWinner));
        }

        private IEnumerator RespawnBall(PlayerRacket playerWinner)
        {
            yield return new WaitForSeconds(0.666f); // hide internet lags with delay

            PhotonNetwork.Destroy(_ballInstance.gameObject);

            foreach (var player in allPlayers)
            {
                player.Master_OnRoundEnded();
            }

            StartGameRound(playerWinner);
        }
    }
}
