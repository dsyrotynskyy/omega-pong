﻿using OmegaPong.Modding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace OmegaPong.Modding
{
    public class UImageFileSprite : AbstractFileSpriteSetuper
    {
        private Image _image;

        protected void Awake()
        {
            _image = GetComponent<Image>();
            SetSprite(CreateSprite ());
        }

        protected void SetSprite(Sprite sprite)
        {
            _image.sprite = sprite;
        }
    }
}
