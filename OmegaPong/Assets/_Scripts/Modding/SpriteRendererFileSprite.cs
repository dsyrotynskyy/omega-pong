﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OmegaPong.Modding
{
    public class SpriteRendererFileSprite : AbstractFileSpriteSetuper
    {
        private SpriteRenderer _spriteRenderer;

        protected void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            SetSprite(CreateSprite ());
        }

        protected void SetSprite(Sprite sprite)
        {
            _spriteRenderer.sprite = sprite;
        }
    }
}
