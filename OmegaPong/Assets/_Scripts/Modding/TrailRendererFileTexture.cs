﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OmegaPong.Modding
{
    public class TrailRendererFileTexture : AbstractFileSpriteSetuper
    {       
        void Start()
        {
            TrailRenderer trailRenderer = GetComponent<TrailRenderer>();
            Material mat = trailRenderer.material;
            mat.SetTexture("_MainTex", CreateTexture());
            trailRenderer.material = mat;
        }
    }
}

