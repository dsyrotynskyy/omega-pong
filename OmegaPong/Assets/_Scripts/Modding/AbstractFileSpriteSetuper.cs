﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace OmegaPong.Modding
{
    public abstract class AbstractFileSpriteSetuper : MonoBehaviour
    {
        public string fileName;
        public int pixelsPerUnit = 100;

        public Texture2D CreateTexture()
        {
            string path = Application.dataPath + "/StreamingAssets/" + fileName;

            byte[] data = File.ReadAllBytes(path);
            Texture2D texture = new Texture2D(64, 64, TextureFormat.ARGB32, false);
            texture.LoadImage(data);
            texture.name = Path.GetFileNameWithoutExtension(path);
            return texture;
        }

        public Sprite CreateSprite()
        {
            Texture2D t = CreateTexture();

            return CreateSprite(t);
        }

        public Sprite CreateSprite(Texture2D texture)
        {
            Sprite s = Sprite.Create(texture, 
                new Rect(0, 0, texture.width, texture.height),
                new Vector2(0.5f, 0.5f),
                pixelsPerUnit);
            return s;
        }
    }
}


