﻿using Kajabity.Tools.Java;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace OmegaPong.Localization
{

    public class LocalizationManager
    {
        private static LocalizationManager _instance = new LocalizationManager();

        public static LocalizationManager GetInstance
        {
            get
            {
                return _instance;
            }
        }

        private LocalizationManager()
        {
            Init();
        }

        protected readonly JavaProperties javaProperties = new JavaProperties();

        void Init()
        {
            string path = Application.dataPath + "/StreamingAssets/" + "localization.txt";

            if (File.Exists(path))
            {
                using (Stream sr = File.OpenRead(path))
                {
                    javaProperties.Load(sr);
                }
            } else
            {
                Debug.LogError("Missing localization file");
            }
        }

        public string LoadString (string key)
        {
            if (!javaProperties.ContainsKey(key))
            {
                Debug.LogError("missing key " + key);
                return "habrakadabra";
            }
            return (string)javaProperties[key];
        }
    }

}
