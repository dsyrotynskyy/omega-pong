﻿using OmegaPong.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OmegaPong.UI
{
    public class UTextLocalizer : MonoBehaviour
    {
        public string id;

        void Start()
        {
            Text t = GetComponent<Text>();
            string localized = LocalizationManager.GetInstance.LoadString (id);
            t.text = localized;
        }
    }
}

