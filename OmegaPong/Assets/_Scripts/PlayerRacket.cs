﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;
using Photon;

namespace OmegaPong
{
    [RequireComponent(typeof(PhotonView))]
    public class PlayerRacket : PunBehaviour
    {
        public PlayerId Id;
        public float speed = 30;       
        public int Score { get; private set; }
        public UnityEvent onScoreChangedEvent = new UnityEvent();

        private string _axis = "Vertical";
        private Rigidbody2D _rigidbody2d;
        private bool _hasMoved;
        private UnityEvent _onHasMoved = new UnityEvent();
        private Vector3 _originalPosition;

        private void Awake()
        {
            _rigidbody2d = GetComponent<Rigidbody2D>();
            _originalPosition = transform.position;
        }

        private void Start()
        {
            if (PhotonNetwork.offlineMode)
            {
                if (Id == PlayerId.Player2)
                {
                    _axis = "Vertical2";
                }
                else
                {
                    _axis = "Vertical1";
                }
            } else
            {
                if (!PhotonNetwork.isMasterClient && Id == PlayerId.Player2)
                {
                    photonView.TransferOwnership(PhotonNetwork.player);
                }
            }
        } 

        public void Master_OnRoundStart()
        {
            photonView.RPC("RPC_OnRoundStarted", PhotonTargets.All);
        }

        [PunRPC]
        private void RPC_OnRoundStarted()
        {
            _hasMoved = false;
        }

        internal void Master_OnRoundEnded()
        {
            photonView.RPC("RPC_OnRoundEnded", PhotonTargets.All);
        }

        [PunRPC]
        private void RPC_OnRoundEnded ()
        {
            transform.position = _originalPosition;
        }

        public void AddOnHasMovedListener(UnityAction onHasMovedAction)
        {
            _onHasMoved.AddListener(onHasMovedAction);
        }

        void FixedUpdate()
        {
            if (!photonView.isMine)
            {
                return;
            }

            float v = Input.GetAxisRaw(_axis);

            _rigidbody2d.velocity = new Vector2(0, v) * speed;

            if (!_hasMoved && v != 0f)
            {
                photonView.RPC("RPC_HasMoved", PhotonTargets.All);
            }
        }

        [PunRPC]
        private void RPC_HasMoved ()
        {
            _onHasMoved.Invoke();
            _onHasMoved.RemoveAllListeners();
            _hasMoved = true;
        }

        internal void Master_OnWonRound()
        {
            photonView.RPC("RPC_WonRound", PhotonTargets.All);
        }

        [PunRPC]
        void RPC_WonRound ()
        {
            Score++;
            onScoreChangedEvent.Invoke();
        }
    }

    public enum PlayerId { Player1 = 1, Player2 = 2 }
}
