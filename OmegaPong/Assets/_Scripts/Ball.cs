﻿using UnityEngine;
using System.Collections;
using System;
using Photon;

namespace OmegaPong
{
    [RequireComponent(typeof(PhotonView))]
    public class Ball : PunBehaviour
    {
        public float speed = 30;
        private Rigidbody2D _rigidbody2d;
        private GameObject _player1;
        private GameObject _player2;
        private WorldManager _worldManager;
        public delegate void OnTouchedPlayerWall(PlayerId wallId);
        public event OnTouchedPlayerWall onTouchedPlayerWall;

        void Awake()
        {
            _rigidbody2d = GetComponent<Rigidbody2D>();
            _worldManager = WorldManager.GetInstance;
            _player1 = _worldManager.GetPlayer(PlayerId.Player1).gameObject;
            _player2 = _worldManager.GetPlayer(PlayerId.Player2).gameObject;
        }

        internal void Master_SetDirection(Vector2 direction)
        {
            _rigidbody2d.velocity = direction * speed;
        }

        float hitFactor(Vector2 ballPos, Vector2 racketPos,
                        float racketHeight)
        {
            // ascii art:
            // ||  1 <- at the top of the racket
            // ||
            // ||  0 <- at the middle of the racket
            // ||
            // || -1 <- at the bottom of the racket
            return (ballPos.y - racketPos.y) / racketHeight;
        }

        void OnCollisionEnter2D(Collision2D col)
        {
            // Note: 'col' holds the collision information. If the
            // Ball collided with a racket, then:
            //   col.gameObject is the racket
            //   col.transform.position is the racket's position
            //   col.collider is the racket's collider

            // Hit the left Racket?
            if (col.gameObject == _player1.gameObject)
            {
                SetVelocityAfterPlayerHit(col, true);
            } else
            if (col.gameObject == _player2.gameObject)
            {
                SetVelocityAfterPlayerHit(col, false);
            }  
        }

        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.CompareTag("PlayerWall"))
            {
                PlayerWall pw = col.gameObject.GetComponent<PlayerWall>();
                if (pw)
                {
                    CheckTouchPlayerWall(pw);
                }
                else
                {
                    Debug.LogError("missing PlayerWall on " + gameObject.name);
                }
            }
        }

        void SetVelocityAfterPlayerHit (Collision2D col, bool isPlayer1)
        {
            if (!photonView.isMine)
            {
                return;
            }

            // Calculate hit Factor
            float y = hitFactor(transform.position,
                                col.transform.position,
                                col.collider.bounds.size.y);

            int len = 1;
            if (!isPlayer1)
            {
                len = -1;
            }

            // Calculate direction, make length=1 via .normalized
            Vector2 dir = new Vector2(len, y).normalized;

            // Set Velocity with dir * speed
            _rigidbody2d.velocity = dir * speed;
        }

        void CheckTouchPlayerWall (PlayerWall pw)
        {
            PlayerId playerId = pw.playerOwnerId;
            PlayerRacket wallOwnerPlayer = _worldManager.GetPlayer(playerId);
            if (wallOwnerPlayer.photonView.isMine)
            {
                int id = (int)playerId;
                photonView.RPC("RPC_OnTouchedPlayerWall", PhotonTargets.MasterClient, id);
            } 
        }

        [PunRPC]
        void RPC_OnTouchedPlayerWall (int id)
        {
            PlayerId playerId = (PlayerId)id;
            if (onTouchedPlayerWall != null)
            {
                onTouchedPlayerWall(playerId);
            }
        }
    }
}
