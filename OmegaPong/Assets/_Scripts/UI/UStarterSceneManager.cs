﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace OmegaPong.UI
{
    public class UStarterSceneManager : MonoBehaviour
    {
        public GameObject WaitNetworkGamePanel;
        public Button _networkGameButton;
        private PhotonGameStarter _photonGameStarter;

        private void Start()
        {
            WaitNetworkGamePanel.SetActive(false);
            _photonGameStarter = PhotonGameStarter.GetInstance;
            InvokeRepeating("CheckIsNetworkGameAvailable", 0f, 0.5f);
        }

        public void ButtonClick_Hotseat()
        {
            _photonGameStarter.LaunchHotSeatGame();
        }

        public void ButtonClick_LaunchNetworkGame()
        {
            bool res = _photonGameStarter.LaunchNetworkGame();
            if (res)
            {
                _networkGameButton.interactable = false; //if we won't disable it, it will blink a few times
                WaitNetworkGamePanel.SetActive(true);
            }
        }

        public void ButtonClick_CancelNetworkGame()
        {
            WaitNetworkGamePanel.SetActive(false);
            _photonGameStarter.CancelNetworkGame();
        }

        private void Update()
        {
            if (Input.GetAxis("Cancel") > 0f)
            {
                Debug.Log("Quit app");
                Application.Quit();
            }
        }

        void CheckIsNetworkGameAvailable ()
        {
            if (WaitNetworkGamePanel.activeInHierarchy) // Check only if network game button is clickable
            {
                return;
            }

            bool isNetworkGameAvailable = this._photonGameStarter.IsNetworkGameAvailable;
            if (_networkGameButton.interactable != isNetworkGameAvailable)
            {
                _networkGameButton.interactable = isNetworkGameAvailable;
            }
        }
    }
}
