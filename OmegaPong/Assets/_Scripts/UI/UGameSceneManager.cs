﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace OmegaPong.UI
{
    public class UGameSceneManager : MonoBehaviour
    {
        PhotonGameSceneManager _photonSceneManager;

        private void Start()
        {
            _photonSceneManager = PhotonGameSceneManager.GetInstance;
        } 

        private void Update()
        {
            if (Input.GetAxis("Cancel") > 0f)
            {
                _photonSceneManager.ExitGameScene();
            }
        }
    }
}


