﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OmegaPong.UI
{
    public class UPlayerScore : MonoBehaviour
    {
        public PlayerId PlayerId;
        private Text _scoreLabel;
        private PlayerRacket _player;
        private WorldManager _worldManager;

        void Start()
        {
            _scoreLabel = GetComponent<Text>();
            _worldManager = WorldManager.GetInstance;
            _player = _worldManager.GetPlayer(PlayerId);
            _player.onScoreChangedEvent.AddListener(DisplayScore);

            DisplayScore();
        }

        private void DisplayScore ()
        {
            _scoreLabel.text = _player.Score.ToString ();
        }
    }
}
