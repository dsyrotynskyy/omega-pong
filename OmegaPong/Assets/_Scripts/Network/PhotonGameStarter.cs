﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OmegaPong {
    [RequireComponent(typeof(PhotonView))]
    public class PhotonGameStarter : UnitySingleton<PhotonGameStarter>
    {
        public string GameSceneName = "_GameScene";

        public bool IsNetworkGameAvailable { get { return PhotonNetwork.connectedAndReady; } }

        private IEnumerator Start()
        {
            if (PhotonNetwork.offlineMode)
            {
                PhotonNetwork.offlineMode = false;
                PhotonNetwork.Disconnect();
                yield return new WaitForSeconds(0.1f);
            }

            PhotonNetwork.sendRate = 100;
            PhotonNetwork.sendRateOnSerialize = 75;

            PhotonNetwork.ConnectUsingSettings("0.1");
            InvokeRepeating("CheckPhotonConnection", 0f, 1f);
        }

        private void CheckPhotonConnection()
        {
            if (!IsNetworkGameAvailable)
            {
                PhotonNetwork.ConnectUsingSettings("0.1");
            }
        }

        internal bool LaunchNetworkGame()
        {
            PhotonNetwork.automaticallySyncScene = true;
            bool res = PhotonNetwork.JoinRandomRoom();
            return res;
        }

        void OnPhotonRandomJoinFailed()
        {
            if (PhotonNetwork.room != null)
            {
                Debug.LogError("Already in room");
                return;
            }
            string RoomName = "Game room " + UnityEngine.Random.Range(int.MinValue, int.MaxValue); //Random room name

            RoomOptions roomOptions = new RoomOptions();
            roomOptions.IsVisible = true;
            roomOptions.IsOpen = true;
            roomOptions.MaxPlayers = 2;
            roomOptions.EmptyRoomTtl = 0;

            PhotonNetwork.CreateRoom(RoomName, roomOptions, PhotonNetwork.lobby);
            PhotonNetwork.playerName = "Player " + UnityEngine.Random.Range(int.MinValue, int.MaxValue);
        }

        public void CancelNetworkGame()
        {
            PhotonNetwork.LeaveRoom();
        }

        public void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            if (newPlayer != PhotonNetwork.player && PhotonNetwork.player.IsMasterClient)
            {
                Room r = PhotonNetwork.room;
                r.IsOpen = false;
                r.IsVisible = false;
                PhotonNetwork.LoadLevel(GameSceneName);
            }
        }

        public void LaunchHotSeatGame()
        {
            PhotonNetwork.Disconnect();
            PhotonNetwork.offlineMode = true;
            PhotonNetwork.LoadLevel(GameSceneName);
        }
    }
}
