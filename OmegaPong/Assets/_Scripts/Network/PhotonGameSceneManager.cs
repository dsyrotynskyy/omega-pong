﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OmegaPong
{
    [RequireComponent(typeof(PhotonView))]
    public class PhotonGameSceneManager : UnitySingleton<PhotonGameSceneManager>
    {
        public string ExitSceneName = "_StartScene";

        PhotonView _photonView;

        private void Start()
        {
            _photonView = GetComponent<PhotonView>();
            PhotonNetwork.automaticallySyncScene = false;
        }

        public void ExitGameScene ()
        {
            PhotonNetwork.LeaveRoom();
            PhotonNetwork.LoadLevel(ExitSceneName);
        }

        public void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
        {
            ExitGameScene();
        }
    }
}

